/* globals document, window, MutationObserver, HTMLFormElement */
(function() {
    'use strict';
    // Register a custom element (x-no-autocomplete) that attempts to disable
    // autocomplete heuristic detection in Chrome. This is achieved by inserting
    // a hidden input element that has an explicit autocomplete type hint.
    // See FormStructure::DetermineHeuristicTypes in Chromium's source code.
    // NOTE: This method inserts a new element as a child to the form element,
    // which may or may not break code that strongly depends on the structure of
    // the child nodes of the form.
    // Usage: <form is="x-no-autocomplete">...</form>

    if (!document.registerElement || !window.MutationObserver) {
        // Required browser features not supported.
        // (this patch is only required for Chrome anyway).
        return;
    }

    var idCounter = 0;
    var extensions = {
        attachedCallback: {
            value: function() {
                // ID to track the element, even after (de)serializing HTML.
                var id = 'x-no-autocomplete-' + (++idCounter);
                var typeHintElem = document.createElement('input');
                typeHintElem.id = id;
                typeHintElem.autocomplete = 'email';
                typeHintElem.style.setProperty('display', 'none', 'important');
                this.appendChild(typeHintElem);

                // Observer to detect removal of the element that is needed to
                // disable autocomplete heuristics.
                var mo = new MutationObserver(function(mutations) {
                    var form = mutations[0].target;
                    if (form.contains(typeHintElem)) {
                        return;
                    }
                    var elemById = form.querySelector('#' + id);
                    if (elemById) {
                        // If someone did something like .innerHTML += ..., then
                        // update the reference (the old reference is obsolete).
                        typeHintElem = elemById;
                        if (typeHintElem.parentNode === form) {
                            // Only return early if the element is still a
                            // direct child node of the form.
                            return;
                        }
                    }
                    form.appendChild(typeHintElem);
                });
                mo.observe(this, {
                    childList: true
                });

                this.xNoAutocompleteData = {
                    id: id,
                    mo: mo
                };
            }
        },
        detachedCallback: {
            value: function() {
                var xNoAutocompleteData = this.xNoAutocompleteData;
                if (!xNoAutocompleteData) {
                    return;
                }
                delete this.xNoAutocompleteData;
                var id = xNoAutocompleteData.id;
                xNoAutocompleteData.mo.disconnect();
                var typeHintElem = id && this.querySelector('#' + id);
                if (typeHintElem) {
                    typeHintElem.remove();
                }
            }
        }
    };

    if (!window.chrome) {
        // The autocomplete hack is only needed for Chrome.
        extensions = {};
    }

    document.registerElement('x-no-autocomplete', {
        prototype: Object.create(HTMLFormElement.prototype, extensions),
        extends: 'form'
    });

    if (typeof define == 'function') {
        define(function () {
            // This module doesn't export anything. Just use
            // var f = document.createElement('form', 'x-no-autocomplete');
            // ... use f as if it were a normal <form>.
        });
    }
})();
